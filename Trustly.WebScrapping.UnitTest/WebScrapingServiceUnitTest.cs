using NUnit.Framework;
using System.Threading.Tasks;
using Trustly.WebScrapping.Business.Services;

namespace Trustly.WebScrapping.UnitTest
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task GetTotalLinksFromSpecificGitRepository()
        {
            var service = new WebScrapingService();
            var links = await service.GetValidLinks("ferreiraapfernanda/web-scraping");

            Assert.AreEqual(5, links.Count);
        }
    }
}