﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Threading.Tasks;
using Trustly.WebScrapping.Business.Interfaces;

namespace Trustly.WebScrapping.Business.Services
{
    public class WhattsAppScrapingService : IWhattsAppScrapingService
    {
        public Task SendMessage(string from, string to, string clientName, string message)
        {

            throw new NotImplementedException();
        }

        public void OpenWebDriver(string user, string number, string message)
        {
            var options = new ChromeOptions();
            options.AddArgument($"user-data-dir=C:\\Users\\{user}\\AppData\\Local\\Google\\Chrome\\User Data");

            IWebDriver chrome = new ChromeDriver(options);
            try
            {

                chrome.Navigate().GoToUrl($"https://api.whatsapp.com/send?phone=55{number}&text={message}");
                chrome.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

                chrome.FindElement(By.XPath("//*[@id='action-button']")).Click();
                chrome.FindElement(By.XPath("//*[@id='fallback_block']/div/div/a")).Click();
                chrome.FindElement(By.XPath("//*[@id=\"main\"]/footer/div[1]/div[3]/button/span")).Click();
            }
            catch (Exception)
            {
                chrome.Quit();
                throw;
            }
            finally
            {
                chrome.Quit();
            }
        }
    }
}
