﻿using ByteSizeLib;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Trustly.WebScrapping.Business.Interfaces;
using Trustly.WebScrapping.Business.Models;

namespace Trustly.WebScrapping.Business.Services
{
    public class WebScrapingService : IWebScrapingService
    {
        public async Task<List<ValidLink>> GetValidLinks(string repo)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var response = await httpClient.GetStringAsync($"https://github.com/{repo}");

                var doc = new HtmlDocument();
                doc.LoadHtml(response);
                var body = doc.DocumentNode.SelectNodes("//body");

                return await GetLinksFromRepository(body);

                //foreach (var link in links)
                //{
                //    await AddFileInfoFormatted(link, files);
                //}
            }
        }

        private async Task<List<ValidLink>> GetLinksFromRepository(HtmlNodeCollection body)
        {
            var validLinks = await Task.FromResult(new List<ValidLink>());
            if (body == null) return validLinks;
            var itens = body.Descendants().Where(x => x.GetAttributeValue("aria-labelledby", "").Equals("files")).ToList();
            if (itens.Any())
            {
                var linkTags = itens.FirstOrDefault().Descendants("a").Where(x => x.GetAttributeValue("id", "") != string.Empty).ToList();
                var links = linkTags.Select(x => HttpUtility.HtmlDecode(x.Attributes["href"].Value)).ToList();
                foreach (var link in links)
                {
                    await AddValidLinks(link, validLinks);
                }


                //Parallel.ForEach(links, l => AddValidLinks(l, validLinks));
            }

            return validLinks;
        }

        private async Task AddValidLinks(string link, List<ValidLink> validLinks)
        {

            if (!link.Contains("tree"))
            {
                validLinks.Add(new ValidLink
                {
                    Uri = link
                });
            }
            else
            {
                await GetLinkTree(link, validLinks);
            }
        }


        private async Task GetLinkTree(string link, List<ValidLink> links)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var response = await httpClient.GetStringAsync($"https://github.com/{link}");

                var doc = new HtmlDocument();
                doc.LoadHtml(response);

                var body = doc.DocumentNode.SelectNodes("//body");

                var moreLinks = await GetLinksFromRepository(body);

                if (moreLinks.Any())
                {
                    links.AddRange(moreLinks);
                }
            }
        }

        public async Task<GitFile> ProcessValidLinks(ValidLink validLink)
        {

            var gitFile = new GitFile()
            {
                Uri = $"https://github.com{validLink.Uri}",
            };
            using (HttpClient httpClient = new HttpClient())
            {
                var response = await httpClient.GetStringAsync($"https://github.com/{validLink.Uri}");

                var doc = new HtmlDocument();
                doc.LoadHtml(response);


                var body = doc.DocumentNode.SelectNodes("//body");
                validLink.Html = body.Descendants("div").Where(x => x.HasClass("text-mono")).FirstOrDefault();

                var fileInfo = validLink.Html?.InnerText.Replace("\n", string.Empty).Trim();
                if (!string.IsNullOrEmpty(fileInfo))
                {
                    int indexOfLines = fileInfo.IndexOf("lines");

                    int lastIndexOfLines = fileInfo.LastIndexOf(")");

                    if (indexOfLines > 0 && lastIndexOfLines > 0)
                    {
                        var numberOfLines = fileInfo.Substring(0, indexOfLines);
                        var fileSize = fileInfo.Substring(lastIndexOfLines + 1).Trim();
                        gitFile.Lines = Convert.ToInt32(numberOfLines);
                        if (fileSize.Contains("Bytes"))
                            gitFile.FileSize = ByteSize.Parse(fileSize.Replace("Bytes", "B"));
                        if (fileSize.Contains("KB"))
                            gitFile.FileSize = ByteSize.Parse(fileSize);
                    }
                    else
                    {
                        gitFile.SizeInfo = fileInfo;
                    }

                    gitFile.Extension = System.IO.Path.GetExtension(gitFile.Uri);

                    return gitFile;

                }
            }

            return null;

        }

    }
}
