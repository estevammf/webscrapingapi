﻿using ByteSizeLib;

namespace Trustly.WebScrapping.Business.Models
{
    public class GroupedFileInfo
    {
        public string Extension { get; set; }
        public int TotalLines { get; set; }
        public string TotalFileSize { get; set; }
    }
}
