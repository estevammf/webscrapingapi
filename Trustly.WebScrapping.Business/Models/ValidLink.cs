﻿using HtmlAgilityPack;

namespace Trustly.WebScrapping.Business.Models
{
    public class ValidLink
    {
        public string Uri { get; set; }
        public HtmlNode Html { get; set; }
    }
}
