﻿using ByteSizeLib;

namespace Trustly.WebScrapping.Business.Models
{
    public class GitFile
    {
        public string Extension { get; set; }
        public string Uri { get; set; }
        public int Lines { get; set; }
        public string SizeInfo { get; set; }
        public ByteSize FileSize { get; set; }
    }
}
