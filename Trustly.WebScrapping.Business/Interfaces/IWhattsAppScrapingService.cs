﻿using System.Threading.Tasks;

namespace Trustly.WebScrapping.Business.Interfaces
{
    public interface IWhattsAppScrapingService
    {
        Task SendMessage(string from, string to, string clientName, string message);
        void OpenWebDriver(string user, string number, string message);
    }
}
