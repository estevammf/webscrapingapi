﻿using Business.Notifications;
using System.Collections.Generic;

namespace Business.Interfaces
{
    public interface INotify
    {
        bool HasNotification();
        List<Notification> GetNotifications();
        void Handle(Notification notification);
        string ToString();
    }
}