﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Trustly.WebScrapping.Business.Models;

namespace Trustly.WebScrapping.Business.Interfaces
{
    public interface IWebScrapingService
    {
        Task<List<ValidLink>> GetValidLinks(string repo);
        Task<GitFile> ProcessValidLinks(ValidLink validLink);
    }
}
