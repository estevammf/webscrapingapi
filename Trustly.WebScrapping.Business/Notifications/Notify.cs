﻿
using Business.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Business.Notifications
{
    public class Notify: INotify
    {
        private List<Notification> _notifications;
        public Notify()
        {
            _notifications = new List<Notification>();
        }

        public void Handle(Notification notification) => _notifications.Add(notification);
        public List<Notification> GetNotifications() => _notifications;
        public bool HasNotification() => _notifications.Any();

        public override string ToString()
        {
            var notifications = string.Join('\n', _notifications.Select(n => n.Message));
            return notifications;
        }
    }
}
