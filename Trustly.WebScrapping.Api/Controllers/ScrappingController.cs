﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Api.Controllers;
using Business.Interfaces;
using ByteSizeLib;
using Dasync.Collections;
using Microsoft.AspNetCore.Mvc;
using Trustly.WebScrapping.Business.Interfaces;
using Trustly.WebScrapping.Business.Models;


namespace Trustly.WebScrapping.Api.Controllers
{
    [Route("api/scraping")]
    [ApiController]
    public class ScrappingController : BaseController
    {
        private readonly IWebScrapingService _service;
        private readonly IWhattsAppScrapingService _whatts;

        public ScrappingController(INotify notify, IWebScrapingService service, IWhattsAppScrapingService whatts) : base(notify)
        {
            _service = service;
            _whatts = whatts;
        }

        /// <summary>
        /// Count total lines and total size of files from an specific repo in GitHub and group by type.
        /// </summary>
        /// <param name="repository">Git repository name Ex: vicenteneto/python-cartolafc</param>
        /// <returns></returns>
        [Route("github")]
        [HttpGet]
        public async Task<ActionResult> GetScrap(string repository)
        {
            try
            {
                var validLinks = await _service.GetValidLinks(repository);

                var files = new List<GitFile>();

                await validLinks.ParallelForEachAsync(async item =>
                {
                    var file = await _service.ProcessValidLinks(item);
                    if (file != null)
                        files.Add(file);
                }, maxDegreeOfParallelism: 10);


                var groupedFiles = files.GroupBy(x => x.Extension).Select(file => new GroupedFileInfo
                {
                    Extension = file.Key,
                    TotalLines = file.Sum(x => x.Lines),
                    TotalFileSize = ByteSize.FromKiloBytes(file.Sum(x => x.FileSize.KiloBytes)).ToString("KB")
                });

            
                return base.CustomResponse(groupedFiles);
            }
            catch (Exception ex)
            {
                base.Notify(ex.Message);
                return base.CustomResponse();
            }
        }

        /// <summary>
        /// Send messages by webwattsapp using selenium web driver
        /// </summary>
        /// <param name="user">%USERPROFILE%\AppData\ - open this folder and get the user data</param>
        /// Ex: C:\Users\C:\Users\estev\AppData\AppData
        /// In this case, the user param is: estev
        /// <param name="number">Destinatary phone number</param>
        /// <param name="message">Message to destinary</param>
        /// <returns></returns>
        [Route("whattsApp")]
        [HttpGet]
        public ActionResult SendWhattsAppMessage(string user = "estev", string number = "", string message = "")
        {
            try
            {

                _whatts.OpenWebDriver(user, number, message);

                return base.CustomResponse("OK");
            }
            catch (Exception ex)
            {
                
                base.NotifyError(ex.Message);
                return base.CustomResponse(GetErrors());
            }
        }
    }
}
