﻿using System.Collections.Generic;
using System.Linq;
using Business.Interfaces;
using Business.Notifications;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace App.Api.Controllers
{
    [ApiController]
    public abstract class BaseController : ControllerBase
    {

        private readonly INotify _notify;
        protected BaseController(INotify notify)
        {
            _notify = notify;

        }

        protected void Notify(string message, NotificationType tipo = NotificationType.Success)
        {
            if (tipo == NotificationType.Error)
                _notify.Handle(new Notification(message));
        }
        protected bool IsValidOperation()
        {
            return !_notify.HasNotification();
        }
        protected List<Notification> GetNotificacoes()
        {
            return _notify.GetNotifications();
        }
        protected string GetErrors()
        {
            var notifications = _notify.ToString();
            return notifications;
        }
        protected ActionResult CustomResponse(object result = null)
        {
            if (IsValidOperation())
            {
                return Ok(result);
            }

            return BadRequest(new ValidationProblemDetails(new Dictionary<string, string[]>
            {
                { "Messages", _notify.GetNotifications().Select(n => n.Message).ToArray() }
            }));
        }

        protected ActionResult CustomResponse(ModelStateDictionary modelState)
        {
            if (!modelState.IsValid) NotifyInvalidModelState(modelState);
            return CustomResponse();
        }

        protected void NotifyInvalidModelState(ModelStateDictionary modelState)
        {
            var erros = modelState.Values.SelectMany(e => e.Errors);
            foreach (var erro in erros)
            {
                var errorMsg = erro.Exception == null ? erro.ErrorMessage : erro.Exception.Message;
                NotifyError(errorMsg);
            }
        }

        protected void NotifyError(string message)
        {
            _notify.Handle(new Notification(message));
        }

        protected enum NotificationType
        {
            Success = 1,
            Error = 2
        }

        protected class Message
        {
            public const string Success = "Ok!";
            public const string Error = "Error!";
        }
    }
}