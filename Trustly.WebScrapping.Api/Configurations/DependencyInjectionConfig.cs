﻿using Business.Interfaces;
using Business.Notifications;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Trustly.WebScrapping.Business.Interfaces;
using Trustly.WebScrapping.Business.Services;

namespace App.Api.Configuration
{
    public static class DependencyInjectionConfig
    {
        public static void ResolveDependencies(this IServiceCollection services)
        {            
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<INotify, Notify>();
            services.AddScoped<IWebScrapingService, WebScrapingService>();
            services.AddScoped<IWhattsAppScrapingService, WhattsAppScrapingService>();

        }

    }
}